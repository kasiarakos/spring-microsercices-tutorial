package com.kasiarakos.microservices.core.api.core.product;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import reactor.core.publisher.Mono;

public interface ProductService {

    @PostMapping(value = "/product", consumes =  "application/json", produces =  "application/json")
    Mono<Product> createProduct(@RequestBody Product product);

    @GetMapping(value = "/product/{productId}", produces =  "application/json")
    Mono<Product> getProduct(
        @PathVariable int productId,
        @RequestParam(value = "delay", required = false, defaultValue = "0") int delay,
        @RequestParam(value = "faultPercent", required = false, defaultValue = "0") int faultPercent);

    @DeleteMapping(value = "/product/{productId}")
    Mono<Void> deleteProduct(@PathVariable int productId);
}