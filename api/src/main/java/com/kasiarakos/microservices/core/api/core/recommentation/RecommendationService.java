package com.kasiarakos.microservices.core.api.core.recommentation;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface RecommendationService {

    @PostMapping(value = "/recommendation", consumes = "application/json", produces =  "application/json")
    Mono<Recommendation> createRecommendation(@RequestBody Recommendation recommendation);

    @GetMapping(value = "/recommendation", produces =  "application/json")
    Flux<Recommendation> getRecommendations(@RequestParam int productId);

    @DeleteMapping("/recommendation")
    Mono<Void> deleteRecommendations(@RequestParam int productId);
}
