package com.kasiarakos.microservices.core.recommendation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;
import com.kasiarakos.microservices.core.api.core.recommentation.Recommendation;
import com.kasiarakos.microservices.core.api.core.recommentation.RecommendationService;
import com.kasiarakos.microservices.core.recommendation.persistence.RecommendationEntity;
import com.kasiarakos.microservices.core.recommendation.persistence.RecommendationRepository;
import com.kasiarakos.microservices.util.exceptions.InvalidInputException;
import com.kasiarakos.microservices.util.http.ServiceUtil;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class RecommendationServiceImpl implements RecommendationService {

    private static final Logger LOG = LoggerFactory.getLogger(RecommendationServiceImpl.class);

    private final ServiceUtil serviceUtil;
    private final RecommendationRepository repository;
    private final RecommendationMapper mapper;

    public RecommendationServiceImpl(ServiceUtil serviceUtil, RecommendationRepository repository, RecommendationMapper mapper) {
        this.serviceUtil = serviceUtil;
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public Mono<Recommendation> createRecommendation(Recommendation recommendation) {

        RecommendationEntity entity = mapper.apiToEntity(recommendation);

        return repository.findByProductIdAndRecommendationId(recommendation.getProductId(), recommendation.getRecommendationId())
                         .hasElement()
                         .filter(exists -> exists)
                         .flatMap(exists -> Mono.error(new InvalidInputException("Duplicate key, Product Id: " + recommendation.getProductId() + ", Recommendation Id:" + recommendation.getRecommendationId())))
                         .then(Mono.just(mapper.apiToEntity(recommendation)))
                         .flatMap(repository::save)
                         .map(mapper::entityToApi);
    }

    @Override
    public Flux<Recommendation> getRecommendations(int productId) {

        if (productId < 1) {
            throw new InvalidInputException("Invalid productId: " + productId);
        }
        return repository.findAllByProductId(productId)
            .log()
            .map(mapper::entityToApi)
            .doOnNext(recommendation -> recommendation.setServiceAddress(serviceUtil.getServiceAddress()));
    }

    @Override
    public Mono<Void> deleteRecommendations(int productId) {
        if (productId < 1) throw new InvalidInputException("Invalid productId: " + productId);
        LOG.debug("deleteRecommendations: tries to delete recommendations for the product with productId: {}", productId);
        return repository.deleteAll(repository.findAllByProductId(productId));
    }
}
