package com.kasiarakos.microservices.core.product.services;

import static reactor.core.publisher.Mono.error;

import java.util.Optional;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;
import com.kasiarakos.microservices.core.api.core.product.Product;
import com.kasiarakos.microservices.core.api.core.product.ProductService;
import com.kasiarakos.microservices.core.product.persistance.ProductEntity;
import com.kasiarakos.microservices.core.product.persistance.ProductRepository;
import com.kasiarakos.microservices.util.exceptions.InvalidInputException;
import com.kasiarakos.microservices.util.exceptions.NotFoundException;
import com.kasiarakos.microservices.util.http.ServiceUtil;

import reactor.core.publisher.Mono;

@RestController
public class ProductServiceImpl implements ProductService {

    private static final Logger LOG = LoggerFactory.getLogger(ProductServiceImpl.class);
    private final Random randomNumberGenerator = new Random();
    private final ServiceUtil serviceUtil;
    private final ProductRepository repository;
    private final ProductMapper mapper;

    public ProductServiceImpl(ServiceUtil serviceUtil, ProductRepository repository, ProductMapper mapper) {
        this.serviceUtil = serviceUtil;
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public Mono<Product> createProduct(Product product) {

        return repository.findByProductId(product.getProductId())
                  .hasElement()
                  .filter(exists -> exists)
                  .flatMap(exists -> Mono.error(new InvalidInputException("Duplicate key, Product Id: " + product.getProductId())))
                  .then(Mono.just(mapper.apiToEntity(product)))
                  .flatMap(repository::save)
                  .map(mapper::entityToApi);
    }

    @Override
    public Mono<Product> getProduct(int productId, int delay, int faultPercent) {

        LOG.debug("/product return the found product for productId={}", productId);
        if (productId < 1) throw new InvalidInputException("Invalid productId: " + productId);

        if (delay > 0) simulateDelay(delay);

        if (faultPercent > 0) throwErrorIfBadLuck(faultPercent);

        return repository.findByProductId(productId)
                        .switchIfEmpty(error(new NotFoundException("No product found for productId: " + productId)))
                        .log()
                        .map(mapper::entityToApi)
                        .doOnNext(e -> e.setServiceAddress(serviceUtil.getServiceAddress()));
    }

    @Override
    public Mono<Void> deleteProduct(int productId) {
        if (productId < 1) throw new InvalidInputException("Invalid productId: " + productId);
        LOG.debug("deleteProduct: tries to delete an entity with productId: {}", productId);
        return repository.findByProductId(productId)
                  .log()
                  .flatMap(repository::delete);
    }

    private void simulateDelay(int delay) {
        LOG.debug("Sleeping for {} seconds...", delay);
        try {Thread.sleep(delay * 1000);} catch (InterruptedException e) {}
        LOG.debug("Moving on...");
    }

    private void throwErrorIfBadLuck(int faultPercent) {
        int randomThreshold = getRandomNumber(1, 100);
        if (faultPercent < randomThreshold) {
            LOG.debug("We got lucky, no error occurred, {} < {}", faultPercent, randomThreshold);
        } else {
            LOG.debug("Bad luck, an error occurred, {} >= {}", faultPercent, randomThreshold);
            throw new RuntimeException("Something went wrong...");
        }
    }

    private int getRandomNumber(int min, int max) {

        if (max < min) {
            throw new RuntimeException("Max must be greater than min");
        }
        return randomNumberGenerator.nextInt((max - min) + 1) + min;
    }
}
