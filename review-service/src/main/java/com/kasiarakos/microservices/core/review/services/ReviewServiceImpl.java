package com.kasiarakos.microservices.core.review.services;

import static java.util.logging.Level.FINE;

import java.util.List;
import java.util.function.Supplier;

import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.RestController;
import com.kasiarakos.microservices.core.api.core.review.Review;
import com.kasiarakos.microservices.core.api.core.review.ReviewService;
import com.kasiarakos.microservices.core.review.persistence.ReviewEntity;
import com.kasiarakos.microservices.core.review.persistence.ReviewRepository;
import com.kasiarakos.microservices.util.exceptions.InvalidInputException;
import com.kasiarakos.microservices.util.http.ServiceUtil;

import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;

@RestController
public class ReviewServiceImpl implements ReviewService {
    private static final Logger LOG = LoggerFactory.getLogger(ReviewServiceImpl.class);

    private final ServiceUtil serviceUtil;
    private final ReviewMapper mapper;
    private final ReviewRepository repository;
    private final Scheduler scheduler;

    public ReviewServiceImpl(ServiceUtil serviceUtil, ReviewMapper mapper, ReviewRepository repository, Scheduler scheduler) {
        this.serviceUtil = serviceUtil;
        this.mapper = mapper;
        this.repository = repository;
        this.scheduler = scheduler;
    }

    @Override
    public Review createReview(Review review) {
        try {
            ReviewEntity entity = mapper.apiToEntity(review);
            ReviewEntity savedEntity = repository.save(entity);

            LOG.debug("createReview: created a review entity: {}/{}", review.getProductId(), review.getReviewId());
            return mapper.entityToApi(savedEntity);

        } catch (DataIntegrityViolationException dive) {
            throw new InvalidInputException("Duplicate key, Product Id: " + review.getProductId() + ", Review Id:" + review.getReviewId());
        }
    }

    @Override
    public Flux<Review> getReviews(int productId) {

        if (productId < 1) throw new InvalidInputException("Invalid productId: " + productId);

        LOG.info("Will get reviews for product with id={}", productId);

        return asyncFlux(() -> Flux.fromIterable(getByProductId(productId))).log(null, FINE);
    }

    protected List<Review> getByProductId(int productId) {

        List<ReviewEntity> entityList = repository.findAllByProductId(productId);
        List<Review> list = mapper.entityListToApiList(entityList);
        list.forEach(e -> e.setServiceAddress(serviceUtil.getServiceAddress()));

        LOG.debug("getReviews: response size: {}", list.size());

        return list;
    }

    @Override
    public void deleteReviews(int productId) {
        LOG.debug("deleteReviews: tries to delete reviews for the product with productId: {}", productId);
        repository.deleteAll(repository.findAllByProductId(productId));
    }

    private <T> Flux<T> asyncFlux(Supplier<Publisher<T>> publisherSupplier) {
        return Flux.defer(publisherSupplier).subscribeOn(scheduler);
    }

}
