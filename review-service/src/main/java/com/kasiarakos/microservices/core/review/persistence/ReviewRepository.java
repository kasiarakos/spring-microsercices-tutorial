package com.kasiarakos.microservices.core.review.persistence;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

@Transactional
public interface ReviewRepository extends CrudRepository<ReviewEntity, Integer> {

    List<ReviewEntity> findAllByProductId(int id);
}
