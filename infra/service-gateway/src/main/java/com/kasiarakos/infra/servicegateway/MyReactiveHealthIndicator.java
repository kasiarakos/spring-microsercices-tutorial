package com.kasiarakos.infra.servicegateway;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.ReactiveHealthIndicator;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

public class MyReactiveHealthIndicator implements ReactiveHealthIndicator {

    private final String url;
    private final WebClient webClient;

    public MyReactiveHealthIndicator(String url, WebClient webClient) {
        this.url = url;
        this.webClient = webClient;
    }

    @Override
    public Mono<Health> health() {
        return webClient.get().uri(url).retrieve().bodyToMono(String.class)
                             .map(s -> new Health.Builder().up().build())
                             .onErrorResume(ex -> Mono.just(new Health.Builder().down(ex).build()))
                             .log();

    }
}