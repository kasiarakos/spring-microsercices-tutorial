package com.kasiarakos.infra.servicegateway;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.CompositeReactiveHealthContributor;
import org.springframework.boot.actuate.health.ReactiveHealthContributor;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;

public class HealthCheckConfiguration {

    private final WebClient.Builder webClientBuilder;

    private WebClient webClient;

    @Autowired
    public HealthCheckConfiguration(WebClient.Builder webClientBuilder) {
        this.webClientBuilder = webClientBuilder;
    }

    @Bean(name = "Core System Microservices")
    ReactiveHealthContributor CoreServicesHealth() {

        Map<String, ReactiveHealthContributor> allIndicators = new HashMap<>();
        allIndicators.put("Product Service", new MyReactiveHealthIndicator("http://product", getWebClient()));
        allIndicators.put("Recommendation Service", new MyReactiveHealthIndicator("http://recommendation", getWebClient()));
        allIndicators.put("Review Service", new MyReactiveHealthIndicator("http://review", getWebClient()));
        allIndicators.put("Product Composier Service", new MyReactiveHealthIndicator("http://product-composite", getWebClient()));

        return CompositeReactiveHealthContributor.fromMap(allIndicators);
    }

    private WebClient getWebClient() {
        if (webClient == null) {
            webClient = webClientBuilder.build();
        }
        return webClient;
    }
}
